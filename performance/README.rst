===================
Performance scripts
===================

This folder contains scripts for measuring the performance of Asap.

The script submitperformanceniflheim.py jobmits the jobs.



Figure (1) on https://asap3.readthedocs.io/en/latest/Performance.html

The first for-loop is used (without threads)

Run with queue=xeon40_clx nodes=40 and all four commands.



Figure (2)

Run with the command 'verletalloy' and the following queues: xeon16,
xeon24, and xeon56 (the 40 node data comes from the figure above).



Figure (3)

The second for loop is used.  queue=xeon40_clx nodes=40 and command
'verletalloy'

