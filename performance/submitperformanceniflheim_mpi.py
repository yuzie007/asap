import subprocess
import os
import sys
import time

queue = 'xeon56'
cores = 56

nodes_list = (1, 2, 3, 4, 6, 8, 10)
#nodes_list = (1,)

template = '''#!/bin/bash -l
#SBATCH --job-name={command}_mpi{cores}_{nodes}
#SBATCH --partition={queue}
#SBATCH -N {nodes}
#SBATCH -n {totalcores}
#SBATCH --time=3:00:00
#SBATCH --output=slurm-%x-%J.out

source "{venv}/bin/activate"

printenv | grep SLURM
printenv | grep OMPI

for i in 3 5 7; do
    time mpiexec python asapperformance.py $i --{command} --mpi
done

'''

venv = os.getenv('VIRTUAL_ENV')
sbatch = ["sbatch"]

command = 'verletalloy'

for nodes in reversed(nodes_list):
    totalcores = nodes * cores
    script = template.format(**locals())
    print("SCRIPT:")
    print(script)
    print()

    sbatchproc = subprocess.Popen(sbatch, stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE, close_fds=True)
    (out, err) = sbatchproc.communicate(script.encode())
    errcode = sbatchproc.wait()
    if errcode:
        print("sbatch failed with error code", str(errcode), file=sys.stderr)
        print("Command line:", sbatch, file=sys.stderr)
        print("Standard error of command:")
        print(err.decode(errors='replace'))
        sys.exit("sbatch failed")
    print(out.decode())
    print()
    # Give SLURM time to put the job into the queue, so they end up in this order
    time.sleep(1)
    

