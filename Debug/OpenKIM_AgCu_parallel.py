from ase.build import bulk
from ase.visualize import view
import numpy as np
from asap3 import *
from asap3.md.velocitydistribution import *
from ase.parallel import world
import sys

#np.random.seed(42)

size = (10, 10, 10)
ismaster = world.rank == 0
cpulayout = (2,1,1)

if ismaster:
    atoms = bulk('Ar', cubic=True).repeat(size)
    alchemize = np.arange(len(atoms)) % 4 == 0
    z = atoms.get_atomic_numbers()
    z[alchemize] = 29  # Cu
    atoms.set_atomic_numbers(z)
    atoms.center(vacuum=25.)
else:
    atoms = None

if world.size > 1:
    atoms = MakeParallelAtoms(atoms, cpulayout)

model = 'LennardJones612_UniversalShifted__MO_959249795837_003'
atoms.set_calculator(OpenKIMcalculator(model, verbose=0))

dyn = Langevin(atoms, timestep=3*units.fs, temperature=800*units.kB,
               friction=0.02)

MaxwellBoltzmannDistribution(atoms, 300*units.kB)

traj = Trajectory('ArCu.traj', "w", atoms)
traj.write()

# Print the energies
logfile = open("ArCu.log", "wt")
def printenergy(a, step=[0,]):
    n = a.get_global_number_of_atoms()
    ekin = a.get_kinetic_energy() / n
    epot = a.get_potential_energy() / n
    line = ("%4d: E_kin = %-8.4f  E_pot = %-8.4f  E_tot = %-9.5f  T = %.1f K" %
            (step[0], ekin, epot, ekin+epot, 2.0/3.0*ekin/units.kB))
    if ismaster:
        print(line, flush=True)
        print(line, file=logfile, flush=True)
    step[0] += 1

    
printenergy(atoms)
dyn.attach(traj, interval=500)
dyn.attach(printenergy, 50, atoms)
dyn.run(1000)

dyn2 = VelocityVerlet(atoms, 3*units.fs)
dyn2.attach(traj, interval=500) 
dyn2.attach(printenergy, 50, atoms)

dyn2.run(5000)
