.. _Running Common Neighbor Analysis:

================================
Running Common Neighbor Analysis
================================

The Common Neighbor Analysis (CNA) uses the local crystal structure to
find defects. In this script a cluster is melted and then cooled down
again so it solidifies. Restricted CNA (looking only for FCC, HCP and
"other" structures) is used to look for atoms sitting in local
crystalline order, and plots are made of a cut through the cluster
using PrimiPlotter.

In the beginning, the cluster is molten, and
nothing in particular is seen. Towards the end of the simulation, it
solidifies and the center of the cluster is seen to contain atoms in
FCC order (white), and a relatively large number of atoms in HCP order
(red). These are stacking faults, created because the cooling was so
fast.  The actual phase transition is not seen, it happens at a temperature
where the thermal vibrations makes it impossible for CNA to determine the
structure.  See the example :ref:`Running Polyhedral Template Matching` for
an alternative method.

This script uses a chain of observers.  The RestrictedCNA object is
attached as an observer to the Langevin dynamics, and is called at
regular intervals.  The PrimiPlotter is then attached to the
RestrictedCNA object, and is called immediately after CNA analysis has
been performed.


.. figure:: ../_images/finalconfig.png
   :figwidth: 350

   A cut through the cluster. FCC atoms are white, HCP atoms red and
   all other atoms are blue.

:git:`~docs/examples/CNAdemo.py`

.. literalinclude:: CNAdemo.py

