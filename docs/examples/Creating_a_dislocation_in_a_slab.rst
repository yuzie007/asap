.. _Creating a dislocation in a slab:

================================
Creating a dislocation in a slab
================================



This is an example of how to set up a dislocation.  It creates a screw
dislocation in a slab with (111) surfaces.  The dislocation is set up
as two partial dislocations separated by a stacking fault.  Common
Neighbor Analysis (CNA) is used to identify the atoms in the
dislocation core.

The system is quite large, almost a million atoms, and is intended for parallel simulations
(see next example).  If you run out of memory during the CNA, either
reduce the system size or skip the CNA and the plotting. 

:git:`~docs/examples/MakeDislocation.py`

.. literalinclude:: MakeDislocation.py

