.. _Plotting while the simulation runs:

==================================
Plotting while the simulation runs
==================================

The ASE provides several ways to plot a simulation.  The most obvious
is the ``view`` command, starting the ase graphical user interface.
It is mainly suitable for interactive use and for plotting a few times
during a simulation.  The view command is illustrated in the example
:ref:`Constant temperature molecular dynamics`.

In this example, the PrimiPlotter is used to plot the system while the
simulation is running.  Plotting is done to a window, and are saved in
GIF files called a0000.gif, a0001.gif etc.  PrimiPlotter is mainly
intended for plotting from scripts, and can write files in various
graphics formats and/or display the plot on the screen.

:git:`~docs/examples/PlottingMD.py`

This does not look particularly beautiful, it is called PrimiPlotter
for a reason!

.. literalinclude:: PlottingMD.py

