.. _Writing output to a Trajectory file:

===================================
Writing output to a Trajectory file
===================================

This is the same simulation as the :ref:`Simple molecular dynamics simulation`,
but instead of writing the energies to the console, the configurations (and energies) are written to a trajectory file.  You can later plot them.  Once the script has completed, you can view the result by running the command

::

  ase gui TrajectoryMD-output.traj

Download and try the script :git:`~docs/examples/TrajectoryMD.py`

.. literalinclude:: TrajectoryMD.py

