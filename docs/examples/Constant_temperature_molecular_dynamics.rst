.. _Constant temperature molecular dynamics:

=======================================
Constant temperature molecular dynamics
=======================================


By replacing the VelocityVerlet dynamics with Langevin dynamics, the
simulation can be made at a specified temperature.  

A small crystal is set up, the atoms are given a velocity drawn from a
Maxwell-Boltzmann distribution, and Langevin dynamics is run first at
800 K and later at 2000K. A plot is made of the system before the
simulation begins, after running at 800 K and after running at 2000K.
The plots are made with the ``view()`` command.

Already at 800K some disordering is seen as atom can
diffuse on the surface, and surface melting is beginning to set in. At
2000K the cluster melts, and sometimes a few atoms evaporate.

:git:`~docs/examples/MD_Cluster3.py`

.. literalinclude:: MD_Cluster3.py

