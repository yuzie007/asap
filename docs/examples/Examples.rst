.. _Examples:

========
Examples
========

.. toctree::
    :hidden:

    Simple_molecular_dynamics_simulation
    Writing_output_to_a_Trajectory_file
    Constant_temperature_molecular_dynamics
    Plotting_while_the_simulation_runs
    Ramping_up_the_temperature
    Running_Common_Neighbor_Analysis 
    Running_Polyhedral_Template_Matching
    Creating_a_dislocation_in_a_slab
    Running_on_parallel_computers 


Here are some examples of how to use Asap.  Running and modifying
these is probably the best way to familiarize yourself with Asap.

*These examples have been tested and updated in April 2016, using Asap version 3.8.18.*


* :ref:`Simple molecular dynamics simulation`

  A simple example of how to set up a block of atoms, and run a short simulation.  Nothing fancy.

* :ref:`Writing output to a Trajectory file`

  How to save the output from a simulation.

* :ref:`Constant temperature molecular dynamics`

  Langevin dynamics makes it possible to do simulations with constant temperature rather than constant energy.

* :ref:`Plotting while the simulation runs`

  On the fly plotting.

* :ref:`Ramping up the temperature` while plotting.  

  A simple example of a more complicated simulation.

* :ref:`Running Common Neighbor Analysis`

  How to run Common Neighbor Analysis (CNA) on a running simulation.

* :ref:`Running Polyhedral Template Matching`

  Same script as above, but using the more robust PTM algorithm
  instead of CNA.

* :ref:`Creating a dislocation in a slab`

  Inspiration on how to set up complicated structures.

* :ref:`Running on parallel computers`
 
  An example illustrating some of the tricks to run Asap in parallel.  


**Please contribute to this page!**
