.. _Installing ASE Asap and GPAW on a Mac:

======================================
Installing ASE, Asap and GPAW on a Mac
======================================

**NEVER install Python packages into the MacOS version of Python, you risk breaking things!**  Instead, install a (more up to date) Python using Homebrew or Anaconda.  This guide explains how to install our software using Homebrew.



**ANACONDA USERS:** If you use anaconda, you will only get the serial version of Asap (which is probably what you need anyway to run in e.g. Jupyter notebooks).  To install with anaconda, first `Install Xcode`_ as described below, then make sure numpy, scipy and matplotlib are installed in Anaconda.  Finally, `Install ASE, Asap and GPAW`_ as described in the very bottom of this document.  Skip all steps in-between.




Install Xcode
=============

Get Xcode from the App Store and install it.  You also need to install
the command line tools, do this with the command::

  xcode-select --install


Install Homebrew
================

These instructions assume that you use the Homebrew package manager to
install open-source software on your Mac.  If you use MacPorts or
fink, you will have to figure out yourself what to do - just follow
the spirit of these instructions.

Install homebrew using the instructions on their home page:
http://brew.sh

Basically, just copy the one-liner near the top of their page into
a Terminal window.

Then you should insert the following lines near the top of your
.bash_profile file to make sure that Homebrew's stuff will take
precedence over the system binaries::

  # Homebrew needs this:
  export PATH=/usr/local/bin:$PATH


Install Homebrew's Python
=========================

Homebrew can use it's own Python installation, or it can use the
system's Python installation.  Using its own is **strongly recommended**,
if you accidentally install something terrible you can always as a
last resort delete everything homebrew-related and start over.  If you
have installed into the system Python installation cleanup after a
mistake can be very difficult.

An alternative would be to use VirtualEnv to compartmentalize you
Python packages.  However, **VirtualEnv does not work with parallel
Asap or GPAW calculations!**

Install Python::

  brew install python

Check that you get Homebrew's python::

  which python

This should give you /usr/local/bin/python, if it gives
/usr/bin/python you should fix your path in your .bash_profile file.


Install ASE prerequisites
=========================

ASE depends heavily on the NumPy package, which can be installed with
Python's default installer ``pip``.  The GUI also requires Matplotlib,
and a few features depend on Scipy as well.  Install them all::

  pip install numpy scipy matplotlib


Install prerequisites for Asap and GPAW
=======================================


You will need openmpi for parallel Asap or GPAW calculations (i.e. if
you wish to use all the CPU cores in your computer).  If you want to
install GPAW, you will also need libxc::

  brew install openmpi
  brew install libxc


Install ASE, Asap and GPAW
==========================

You can now install ASE, Asap and GPAW following the normal
installation procedure.  The easiest is to use pip to install them::

  pip install ase
  pip install asap3
  pip install gpaw


**Note: You do not need Asap to run GPAW.  If you want to do DFT
calculations it is unlikely that you need Asap!**     

**Note: You do not need GPAW to run Asap.  If you want to run
molecular dynamics using the EMT potential you probably do not
need GPAW.**

If you installed GPAW, you need to `install its PAW setups`_ too

.. _`install its PAW setups`: https://wiki.fysik.dtu.dk/gpaw/dev/setups/setups.html#installation-of-paw-datasets
