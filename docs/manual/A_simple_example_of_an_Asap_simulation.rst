.. _A simple example of an Asap simulation:

======================================
A simple example of an Asap simulation
======================================

The following script sets up a 500-atom cluster of atoms in the shape
of a small crystallite, and then performs a constant-temperature
molecular dynamics simulation at 800K using the Langevin algorithm.
After a while, the temperature is increased to 2000K.  The potential,
kinetic and total energies are printed every 100 timestep, and the
atoms are written to a trajectory file every 2000 timesteps:


.. literalinclude:: ../examples/MD_Cluster3.py


You can also download :git:`~docs/examples/MD_Cluster3.py`
