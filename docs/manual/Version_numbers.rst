.. _Version numbers:

====================
Asap version numbers
====================

.. contents::

An Asap version number (e.g. 3.13.0) contains three parts.

- The major version number (3 in this example) will only change in case of major changes to the interface, making your scripts incompatible.

- The second version number (13 in this example) is even in production versions of Asap, and odd in development versions.

- The last version number marks corrections of minor bugs and small improvents in the case of a production version, but may mark more significant changes in development versions.

Checking the version number
===========================

... of the source code
----------------------

In the source directory, you can check the version number with ``make version``::

  [demokrit] ~/development/asap>make version
  ASAP version 3.13.0

... of the compiled Asap
------------------------

The ``PrintVersion`` command prints the version number, including information about the compiler options used when compiling::

  >>> import asap3
  >>> asap3.print_version()
  ASAP version 3.13.0 parallel, compiled Mar 30 2023 15:41:33 on demokrit using 'env LAMMPICXX='g++' OMPI_MPICXX='g++' mpicxx -O3 -pipe -g -Wall -Wno-sign-compare -Wno-unused-function -Wno-write-strings  -DWITH_OPENKIM'


If an argument is given, information about which files where loaded is printed::

  >>> asap3.print_version(1)
  ASAP version 3.13.0 parallel, compiled Mar 30 2023 15:41:33 on demokrit using 'env LAMMPICXX='g++' OMPI_MPICXX='g++' mpicxx -O3 -pipe -g -Wall -Wno-sign-compare -Wno-unused-function -Wno-write-strings  -DWITH_OPENKIM'
    Python module:  /Users/schiotz/development/asap/Python/asap3/__init__.py
    C++ module:     /Users/schiotz/development/asap/x86_64/_asap.so
    ase module:     /Users/schiotz/development/ase/ase/__init__.py

    Python version: 3.11.3 (main, Apr  7 2023, 19:25:52) [Clang 14.0.0 (clang-1400.0.29.202)]
    numpy version:  1.23.5
    ase version:    3.23.0b1


... in a script
---------------

The same string printed by ``print_version()`` is returned by ``get_version()``.  The version number itself is available as ``__version__``.

