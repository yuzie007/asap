.. _Brenner:

=====================
The Brenner potential
=====================

Brenner's reactive bond order potential for Carbon, hydrocarbons,
Silicon and Germanium.  The potential is mainly intended for teaching,
where it is used for simulating carbon nanotubes.  It is not otherwise
tested.

The code is `Peter McCluskey's C implementation`__ of the Brenner
potential, adapted with permission for use in Asap by David Landis.

__ http://www.bayesianinvestor.com/brenner/index.html
