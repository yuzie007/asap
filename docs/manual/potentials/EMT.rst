.. _EMT:

=============================
Effective Medium Theory (EMT)
=============================

The EMT potential is the main Calculator in Asap, and the *raison
d'être* of Asap.  It is a many-body potential, giving a good
description of the late transition metals crystalling in the FCC
crystal structure.  The elements described by the main set of EMT
parameters are Al, Ni, Cu, Pd, Ag, Pt, and Au, the Al potential is
however not suitable for materials science application, as the
stacking fault energy is wrong.

In addition to the default EMT parameters, it is possible to use a
``ParameterProvider`` to provide an alternative set of EMT
parameters.  In this way, it is possible to use EMT potentials for Ru
as well as for Mg-Cu and Cu-Zr metallic glasses.  

EMT Potential parameters
========================

EMTStandardParameters
  The default EMT parameters, as published in [3].

EMTRasmussenParameters
  An alternative set of parameters for Cu, Ag and Au, reoptimized to
  experimental data including the stacking fault energies by Torben
  Rasmussen.

EMThcpParameters
  EMT parameters for pure HCP metals.  Currenlty, only Ruthenium is
  supported [6].  **Do not use this potential, it is really bad!**

EMTMetalGlassParameters
  EMT parameters for MgCu and CuZr metallic glasses [4, 5].

Examples:
---------


A calculation with default EMT parameters::

  atoms.set_calculator(EMT())

A calculation with Torben Rasmussen's parameters::

  atoms.set_calculator(EMT(EMTRasmussenParameters()))

A calculation for CuZr metallic glass::

  atoms.set_calculator(EMT(EMTMetalGlassParameters()))


Equations
=========

The equations implemented in the EMT potential are documented here:
:git:`~docs/manual/potentials/emt.pdf`.
 
  
References
==========

[1]
   K. W. Jacobsen, J. K. Nørskov and M. J. Puska: "Interatomic
   interactions in the effective-medium theory", *Phys. Rev. B* **35**,
   7423 (1987).

   This is the original paper presenting EMT.  The detailed formulation has
   changed somewhat since.

[2]
   K. W. Jacobsen: "Bonding in metallic systems: an effective medium approach",
   *Comm. Cond. Matter Phys.* **B14**, 129 (1988)

   A paedagogical exposition of the ideas behind EMT.

[3]
   K. W. Jacobsen, P. Stoltze and J. K. Nørskov: "A semi-empirical
   effective medium theory for metals and alloys", *Surf. Sci.*
   **366**, 394 (1996).

   The modern formulation of EMT.  The Asap implementation follows
   this formulation, and the default EMT parameters are the ones
   published here.

[4] 
   N. P. Bailey, J. Schiøtz and K. W. Jacobsen: "Simulation of Cu-Mg
   Metallic Glass: Thermodynamics and Structure", *Phys. Rev. B* **69**,
   144205 (2004).

   The parameters for the MgCu metallic glass was published here.

[5]
   A. Paduraru, A. Kenoufi, N. P. Bailey and J. Schiøtz: "An
   Interatomic Potential for Studying CuZr Bulk Metallic Glasses",
   *Adv. Eng. Mater.* **9**, 505 (2007). 

   The parameters for the CuZr metallic glass.

[6]
   J. Gavnholt and J. Schiøtz: The structure and reactivity of
   Ruthenium nanoparticles, *Phys. Rev. B* **77**, 035404 (2008).

   The parameters for Ru are given in endnote 21.
