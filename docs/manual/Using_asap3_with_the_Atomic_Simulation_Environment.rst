.. _Using asap3 with the Atomic Simulation Environment:

===================================================
Using asap3 with the Atomic Simulation Environment
===================================================

Asap version 3 is a collection of drop-in calculators for the Atomic
Simulation Environment (ASE_).  In addition, a number of analysis
tools are provided, and tools for parallel simulations.

Importing Asap
==============

Most of Asap's objects are importable directly from the main module::

  from ase import Atoms
  from asap3 import EMT, FullNeighborLists

Using the Asap calculators
==========================

Asap calculators are used as any other calculator in ase, this is best
demonstrated by an example, see :ref:`A simple example of an Asap simulation`.  See also the list of supported calculators
(:ref:`Potentials`).

Special Asap versions of ASE objects
====================================

Some standard ASE objects are replaced by Asap's own versions, either
for performance reasons or to support Asap's parallelization paradigm
(see :ref:`Parallel simulations on clusters`).

In particular, be aware of special versions of the dynamics, energy
minimization, constraints and I/O classes.

Special Molecular Dynamics classes
----------------------------------

The following Molecular Dynamics objects should be imported from Asap
rather than from ASE::

  from asap3.md.verlet import VelocityVerlet 
  from asap3.md.langevin import Langevin
  from asap3.md.npt import NPT
  from asap3.md.nvtberendsen import NVTBerendsen 
  from asap3.md.nptberendsen import NPTBerendsen 
  
Energy minimization classes
---------------------------

Most QuasiNewton energy minimization classes will not work well with
Asap, as Asap simulations typically contains thousands of atoms, and
these algorithms construct a 3N-by-3N matrix where N is the number of
atoms.  The exceptions are LBFGS and LBFGS_Linesearch which work for
*serial simulations only*.  Otherwise, please use FIRE or possibly
MDmin::

  from asap3.optimize.fire import FIRE
  from asap3.optimize.mdmin MDMin



File I/O
========

As file input and output is done through ASE, all normal ASE methods work in serial
simulations.  In parallel simulations, data needs to be collected from the 
many MPI tasks.  A special version of the ``Trajectory`` class has been made for
this::

  from asap3.io.trajectory import Trajectory

When attached to a dynamics object, it will output data from both serial and
parallel simulations correctly.  See also the example :ref:`Running on parallel computers`. 

Constraints
===========

Most ASE constraints work only in serial simulations.  The ``FixAtoms``
constraint is both sensible and useful in large scale simulation, and 
a special version has been made::

  from asap3.constraints import FixAtoms

It is recommended to use masks rather than indices when creating the constraint,
as that is less ambiguous.  The indices will be interpreted as indices local to
this task.

Constraints in ASAP are only minimally tested, use at your own peril.


.. _ASE: https://wiki.fysik.dtu.dk/ase/
