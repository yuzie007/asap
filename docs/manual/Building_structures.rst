.. _Building structures:

===================
Building structures
===================

When setting up the structures that you plan to simulate using Asap,
it is most common to use the various ASE modules for this task.  See
the following chapters in the ASE manual:

* `Building things`_:  This is the main ASE Manual chapter on building
  structures.

* `General crystal structures and surfaces`_:  Methods for generating
  crystals with specific orientations.  Particularly useful for large
  chunks of crystals.  For smaller systems, consider using the
  `ase.build.bulk`_ method described in the chapter above.

* `Nanoparticles and clusters`_: For particles (surprise, surprise ...)

In addition, Asap contains a few more specialized functions with a
special focus on Materials Physics:

* :ref:`Creating nanocrystalline materials`

* :ref:`Creating dislocations`



.. _`Building things`: https://wiki.fysik.dtu.dk/ase/ase/build/build.html
.. _`General crystal structures and surfaces`: https://wiki.fysik.dtu.dk/ase/ase/lattice.html
.. _`ase.build.bulk`: https://wiki.fysik.dtu.dk/ase/ase/build/build.html#common-bulk-crystals
.. _`Nanoparticles and clusters`: https://wiki.fysik.dtu.dk/ase/ase/cluster/cluster.html
