.. _Manual:

======
Manual
======

.. toctree::
    :maxdepth: 1

    Introduction

Quick manual for ASE users

.. toctree::
    :maxdepth: 1

    Using_asap3_with_the_Atomic_Simulation_Environment
    A_simple_example_of_an_Asap_simulation

Main manual

.. toctree::
    :maxdepth: 1

    Potentials
    Molecular_dynamics_with_Asap_and_ASE
    Monte_Carlo_simulations
    Simulations_on_parallel_computers
    Parallel_simulations_on_clusters
    Multi-threaded_parallelization
    Input_and_output
    Building_structures
    Creating_nanocrystalline_materials
    Creating_dislocations
   
Data analysis

.. toctree::
    :maxdepth: 1

    On-the-fly_plotting_of_atoms
    Data_analysis_with_observers
    Radial_Distribution_Functions
    Local_crystalline_order
    Neighbor_lists

Advanced topics

.. toctree::
    :maxdepth: 1

    Communication_in_parallel_simulations
    Contribute_to_the_documentation_or_the_code
    Bugs 

Miscellaneous

.. toctree::
    :maxdepth: 1

    Version_numbers

To be written

   * Fields defined on the atoms (stresses, energies, ...)
   * Using asap with GPAW and other calculators
