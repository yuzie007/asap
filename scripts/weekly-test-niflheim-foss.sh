#!/bin/bash -l
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=schiotz@fysik.dtu.dk
#SBATCH --partition=xeon24el8
#SBATCH -N 1
#SBATCH -n 2
#SBATCH --time=04:00:00
#SBATCH --job-name=asap-foss-nightly
#SBATCH --dependency=singleton

set -e   # Exit on error

echo "Job started:" `date`
echo "This is the SLOW (weekly) version."
echo "Working directory:" `pwd`
echo "CPU_ARCH=$CPU_ARCH"
echo "hostname: `hostname`"

asap/scripts/nightly-test.sh . foss slow

echo "Job ended SUCCESSFULLY:" `date`
