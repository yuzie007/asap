Effective Medium Theory (EMT) model based on the EMT implementation in ASAP (https://wiki.fysik.dtu.dk/asap).  This model uses the EMT_Asap__MD_128315414717 model driver.

Effective Medium Theory is a many-body potential of the same class as Embedded Atom Method, Finnis-Sinclair etc.  The main term in the energy per atom is the local density of atoms.

The functional form implemented here is that of Ref. 1.  The principles behind EMT are described in Refs. 2 and 3 (with 2 being the more detailed and 3 being the most pedagogical).  Be aware that the functional form and even some of the principles have changed since refs 2 and 3.  EMT can be considered the last step of a series of approximations starting with Density Functional Theory, see Ref 4.

This model implements the "official" parametrization as published in Ref. 1.

Note on the cutoff: EMT uses a global cutoff, and this cutoff depends on the largest atom in the simulation.  In OpenKIM the model does not reliably have access to all the species in a parallel simulation, so the cutoff is always set to the cutoff associated with the largest supported atom (in this case Silver).

For single-element simulations, please use the single-element parametrizations, as they use a cutoff more appropriate for the element in question (and are marginally faster).

These files are based on Asap version $version.


REFERENCES:

[1] Jacobsen, K. W., Stoltze, P., & Nørskov, J.: "A semi-empirical effective medium theory for metals and alloys". Surf. Sci. 366, 394–402  (1996).

[2] Jacobsen, K. W., Nørskov, J., & Puska, M.: "Interatomic interactions in the effective-medium theory". Phys. Rev. B 35, 7423–7442 (1987).

[3] Jacobsen, K. W.: "Bonding in Metallic Systems: An Effective-Medium Approach".  Comments Cond. Mat. Phys. 14, 129-161 (1988).

[4] Chetty, N., Stokbro, K., Jacobsen, K. W., & Nørskov, J.: "Ab initio potential for solids". Phys. Rev. B 46, 3798–3809 (1992).


HISTORY:

* This model was previously available as MO_118428466217_002.  After the change to KIM API v2 the cutoff is handled in a marginally different way, and a new KIM model ID was assigned.


