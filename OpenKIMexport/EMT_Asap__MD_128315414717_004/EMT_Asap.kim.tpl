#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the Common Development
# and Distribution License Version 1.0 (the "License").
#
# You can obtain a copy of the license at
# http://www.opensource.org/licenses/CDDL-1.0.  See the License for the
# specific language governing permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each file and
# include the License file in a prominent location with the name LICENSE.CDDL.
# If applicable, add the following below this CDDL HEADER, with the fields
# enclosed by brackets "[]" replaced with your own identifying information:
#
# Portions Copyright (c) [yyyy] [name of copyright owner]. All rights reserved.
#
# CDDL HEADER END
#

#
# Copyright (c) 2012, Regents of the University of Minnesota.  All rights reserved.
#
# Contributors:
#    Ryan S. Elliott
#    Ellad B. Tadmor
#    Valeriu Smirichinski
#    Jakob Schiotz, Technical University of Denmark




#######################################################################################################
MODEL_INPUT:
# Name                      Type         Unit                Shape              Requirements

numberOfParticles           integer      none                []

numberOfSpecies             integer      none                []

numberContributingParticles integer      none                []                 optional

particleSpecies             integer      none                [numberOfParticles]

coordinates                 double       length              [numberOfParticles,3]

boxSideLengths              double       length              [3]                optional
get_neigh                   method       none                []                 optional
neighObject                 pointer      none                []                 optional


#######################################################################################################
MODEL_OUTPUT:
# Name                      Type         Unit                Shape              Requirements

cutoff                      double       length              []

energy                      double       energy              []                 optional

particleEnergy              double       energy              [numberOfParticles]   optional

forces                      double       force               [numberOfParticles,3] optional

virial						double		 energy				 [6]				optional

particleVirial				double		 energy				 [numberOfParticles,6] optional

#######################################################################################################
MODEL_PARAMETERS:
# Name                      Type         Unit                Shape              Requirements

# --- No published parameters ---
